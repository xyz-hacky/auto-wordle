document.querySelectorAllShadows = function (selector, el = document.body) {
	const childShadows = Array.from(el.querySelectorAll('*'))
		.map((el) => el.shadowRoot)
		.filter(Boolean);

	const childResults = childShadows.map((child) => document.querySelectorAllShadows(selector, child));

	const result = Array.from(el.querySelectorAll(selector));
	return result.concat(childResults).flat();
};

class AutoWordle {
	constructor() {
		this.cacheKey = 'nyt-wordle-solutions';
    this.resultKeys = [
      'nyt-wordle-moogle/ANON',
      'nyt-wordle-refresh',
      'nyt-wordle-statistics',
    ];
		this.result = undefined;
		this.ready = undefined;
	}

	async initialize() {
		let cache = {};
		try {
			cache = localStorage.hasOwnProperty(this.cacheKey) ? JSON.parse(localStorage.getItem(this.cacheKey)) : cache;
		} catch (e) {
			console.log('Load Cache Error', e);
		}

		const date = new Date();
		const year = date.toLocaleString('default', { year: 'numeric' });
		const month = date.toLocaleString('default', { month: '2-digit' });
		const day = date.toLocaleString('default', { day: '2-digit' });
		const dateStr = `${year}-${month}-${day}`;

		try {
			this.result = cache?.[dateStr]
				? cache[dateStr]
				: await fetch(`https://www.nytimes.com/svc/wordle/v2/${dateStr}.json`).then((res) => res.json());
			this.ready = true;
		} catch (e) {
			console.log('Initialize error:', e);
			this.ready = false;
		} finally {
			cache[dateStr] = this.result;
			localStorage.setItem(this.cacheKey, cache);
		}
	}

	find() {
		return this.result?.solution;
	}

  retry() {
    this.resultKeys.forEach((key) => {
      if (localStorage.hasOwnProperty(key)) {
        localStorage.removeItem(key)
      }
    });
  }

	solve() {
		if (!this.ready) {
			alert('Auto Wordle does not initialize sucessfully.');
			return;
		}

		const word = this.find();
		if (!word) {
			return alert(`Error! Word does not found, please wait for plugin updates.`);
		}

		return word.split('').forEach((letter, i) => {
			document.querySelectorAllShadows(`button[data-key="${letter}"]`)[0]?.click();
			if (i === 4) document.querySelectorAllShadows('button[data-key="↵"]')[0]?.click();
		});
	}
}
