chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  const autoWordle = new AutoWordle();

  switch (request.command ) {
    case 'solve': 
      return autoWordle.initialize().then(() => {
        autoWordle.solve();
      });
    case 'retry':
      return autoWordle.initialize().then(() => {
        autoWordle.retry();
      });
  }
});