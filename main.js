const solveBtn = document.getElementById('solve');
const retryBtn = document.getElementById('retry');

solveBtn.addEventListener('click', () => {
	chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
		chrome.tabs.sendMessage(tabs[0].id, { command: 'solve' });
	});
});
retryBtn.addEventListener('click', () => {
	chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
		chrome.tabs.sendMessage(
      tabs[0].id,
      { command: 'retry' },
      undefined,
      chrome.tabs.reload(tabs[0].id, { bypassCache: true }),
    );
	});
});
